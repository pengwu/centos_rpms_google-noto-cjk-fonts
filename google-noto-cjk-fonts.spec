%global sans_version 2.004
%global serif_version 2.002

%global fontname google-noto-cjk
%global fontconf google-noto
%global fontconf2 65-%{fontconf}-cjk-fonts.conf

%global common_desc \
Noto CJK fonts, supporting Simplified Chinese, Traditional Chinese, \
Japanese, and Korean. The supported scripts are Han, Hiragana, Katakana, \
Hangul, and Bopomofo. Latin, Greek, Cyrllic, and various symbols are also \
supported for compatibility with CJK standards. \
%{nil}

Name:           google-noto-cjk-fonts
Version:        20230817
Release:        1%{?dist}
Summary:        Google Noto Sans CJK Fonts

License:        OFL
URL:            https://github.com/notofonts/noto-cjk
Source0:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/03_NotoSansCJK-OTC.zip
Source2:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/05_NotoSansCJK-SubsetOTF.zip
Source3:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/11_NotoSansMonoCJKjp.zip
Source4:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/12_NotoSansMonoCJKkr.zip
Source5:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/13_NotoSansMonoCJKsc.zip
Source6:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/14_NotoSansMonoCJKtc.zip
Source7:        https://github.com/notofonts/noto-cjk/releases/download/Sans%{sans_version}/15_NotoSansMonoCJKhk.zip

Source10:       https://github.com/notofonts/noto-cjk/releases/download/Serif%{serif_version}/04_NotoSerifCJKOTC.zip
Source12:       https://github.com/notofonts/noto-cjk/releases/download/Serif%{serif_version}/06_NotoSerifCJKSubsetOTF.zip

Source21:       genfontconf.py
Source22:       genfontconf.sh
Source23:       %{fontconf2}

Source30:       README.md

BuildArch:      noarch
BuildRequires:  fontpackages-devel
BuildRequires:  python3
BuildRequires:  /usr/bin/xmllint
Requires:       fontpackages-filesystem
Requires:       google-noto-sans-cjk-ttc-fonts
Requires:       google-noto-serif-cjk-ttc-fonts

%if 0%{?fedora}

Obsoletes:      google-noto-sans-cjk-fonts < 20150617
Provides:       google-noto-sans-cjk-fonts = 20150617

# notocjkrep Package Name
%define notocjkrep(:)\
%define pname %(echo %{*} | tr "A-Z " "a-z-")\
Obsoletes:      google-noto-%{pname}-fonts < 20150617\
Provides:       google-noto-%{pname}-fonts = 20150617\
Obsoletes:      google-noto-cjk-%{pname}-fonts < %{version}-%{release}\
Provides:       google-noto-cjk-%{pname}-fonts = %{version}-%{release}\


%notocjkrep Sans Simplified Chinese
%notocjkrep Sans Traditional Chinese
%notocjkrep Sans Japanese
%notocjkrep Sans Korean

%endif


%description
%common_desc

%package common
Summary:        Common files for Noto CJK fonts

%description common
%common_desc

Common files for Google Noto CJK fonts.


# notocjkpkg [-n sub-package-name] [-f font-file] [-p priority] Font Name
# -n sub package name
# -f font file name
# -p overrides fontconfig .conf priority (default 66)
%define notocjkpkg(n:f:p:) \
# override _font_pkg_name to avoid name changes in _font_pkg \
%define _font_pkg_name() %1 \
%define subpkgname %{-n:%{-n*}} \
%define fontfiles %{-f:%{-f*}}\
%define fconf %{-p*}%{!-p:66}-%{fontconf}-%{subpkgname}.conf\
%package -n     google-noto-%subpkgname-fonts \
Summary:        %* font files for %{name} \
Requires:       %{name}-common = %{version}-%{release} \
\
%description -n google-noto-%subpkgname-fonts \
%common_desc \
\
The google-noto-%subpkgname-fonts package contains %* fonts. \
\
%_font_pkg -n google-noto-%subpkgname-fonts -f %{fconf} %fontfiles \
%{nil}


%notocjkpkg -n sans-cjk-ttc -f NotoSansCJK-*.ttc -p 65-0 Sans OTC


%notocjkpkg -n serif-cjk-ttc -f NotoSerifCJK-*.ttc -p 65-0 Serif OTC


%notocjkpkg -n sans-mono-cjk-jp -f NotoSansMonoCJKjp-*.otf Japanese Multilingual Sans Mono OTF


%notocjkpkg -n sans-mono-cjk-kr -f NotoSansMonoCJKkr-*.otf Korean Multilingual Sans Mono OTF


%notocjkpkg -n sans-mono-cjk-sc -f NotoSansMonoCJKsc-*.otf Simplified Chinese Multilingual Sans Mono OTF


%notocjkpkg -n sans-mono-cjk-tc -f NotoSansMonoCJKtc-*.otf Traditional Chinese Multilingual Sans Mono OTF


%notocjkpkg -n sans-mono-cjk-hk -f NotoSansMonoCJKhk-*.otf Traditional Chinese Multilingual Sans Mono OTF


%notocjkpkg -n sans-jp -f NotoSansJP-*.otf Japanese Region-specific Sans OTF


%notocjkpkg -n serif-jp -f NotoSerifJP-*.otf Japanese Region-specific Serif OTF


%notocjkpkg -n sans-kr -f NotoSansKR-*.otf Korean Region-specific Sans OTF


%notocjkpkg -n serif-kr -f NotoSerifKR-*.otf Korean Region-specific Serif OTF


%notocjkpkg -n sans-sc -f NotoSansSC-*.otf Simplified Chinese Region-specific Sans OTF


%notocjkpkg -n serif-sc -f NotoSerifSC-*.otf Simplified Chinese Region-specific Serif OTF


%notocjkpkg -n sans-tc -f NotoSansTC-*.otf Traditional Chinese Region-specific Sans OTF


%notocjkpkg -n serif-tc -f NotoSerifTC-*.otf Traditional Chinese Region-specific Serif OTF


%notocjkpkg -n sans-hk -f NotoSansHK-*.otf Traditional Chinese Region-specific Sans OTF


%prep
%setup -q -c

for zipfile in `ls %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} %{SOURCE7} %{SOURCE10} %{SOURCE12}`;
do unzip -j $zipfile -x LICENSE;
done

cp -p %{SOURCE21} %{SOURCE22} .
# generate the font conf files
bash -x ./genfontconf.sh

cp -p %{SOURCE30} .

%build


%install
install -m 0755 -d %{buildroot}%{_fontdir}

# copy OTC files
install -m 0644 -p NotoSansCJK-*.ttc %{buildroot}%{_fontdir}
install -m 0644 -p NotoSerifCJK-*.ttc %{buildroot}%{_fontdir}

# copy Multilingual OTF files
install -m 0644 -p NotoSansMonoCJK{jp,kr,sc,tc,hk}-*.otf %{buildroot}%{_fontdir}

# copy Region-specific OTF
install -m 0644 -p NotoSans{JP,KR,SC,TC,HK}-*.otf %{buildroot}%{_fontdir}
install -m 0644 -p NotoSerif{JP,KR,SC,TC}-*.otf %{buildroot}%{_fontdir}


install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
            %{buildroot}%{_fontconfig_confdir}

for f in sans-cjk-ttc serif-cjk-ttc \
    sans-mono-cjk-jp \
    sans-mono-cjk-kr \
    sans-mono-cjk-sc \
    sans-mono-cjk-tc \
    sans-mono-cjk-hk \
    sans-jp serif-jp \
    sans-kr serif-kr \
    sans-sc serif-sc \
    sans-tc serif-tc \
    sans-hk;
do
    fconf=$(basename -a *-%{fontconf}-$f.conf)
    if [ "$(echo $fconf | wc -w)" -ne 1 ]; then
       echo "Did not find unique \*-%{fontconf}-$f.conf file"
       exit 1
    fi

    install -m 0644 -p ${fconf} \
                %{buildroot}%{_fontconfig_templatedir}/${fconf}

    ln -s %{_fontconfig_templatedir}/${fconf} \
         %{buildroot}%{_fontconfig_confdir}/${fconf}
done

install -m 0644 -p %{SOURCE23} \
            %{buildroot}%{_fontconfig_templatedir}/%{fontconf2}

ln -s %{_fontconfig_templatedir}/%{fontconf2} \
     %{buildroot}%{_fontconfig_confdir}/%{fontconf2}


%files


%files common
%doc README.md
%license LICENSE
%{_fontconfig_templatedir}/%{fontconf2}
%config(noreplace) %{_fontconfig_confdir}/%{fontconf2}


%changelog
* Thu Nov  2 2023 Peng Wu <pwu@redhat.com> - 20230817-1
- Update Noto CJK to Sans 2.004 and Serif 2.002
- Drop some Noto CJK Language Specific OTFs sub packages
- Resolves: RHEL-13738

* Mon Aug 09 2021 Mohan Boddu <mboddu@redhat.com> - 20201206-4
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Thu Apr 15 2021 Mohan Boddu <mboddu@redhat.com> - 20201206-3
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 20201206-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Jan 18 2021 Peng Wu <pwu@redhat.com> - 20201206-1
- Update to v20201206

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 20190416-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 20190416-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Tue Aug 13 2019 Peng Wu <pwu@redhat.com> - 20190416-5
- Update 65-google-noto-cjk-fonts.conf for HK

* Thu Aug  1 2019 Peng Wu <pwu@redhat.com> - 20190416-4
- Correct lang property of fontconfig in Noto Sans CJK fonts

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 20190416-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Tue May 14 2019 Peng Wu <pwu@redhat.com> - 20190416-2
- Include HongKong fonts

* Wed Apr 17 2019 Peng Wu <pwu@redhat.com> - 20190416-1
- Update to git commit be6c059

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 20170602-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Fri Dec 14 2018 Peng Wu <pwu@redhat.com> - 20170602-9
- Support Macau locale

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 20170602-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Mon Apr 16 2018 Peng Wu <pwu@redhat.com> - 20170602-7
- Make Noto CJK OTC files as default CJK fonts

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 20170602-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Jan 22 2018 Akira TAGOH <tagoh@redhat.com> - 20170602-5
- Update the priority to change the default font to Noto.

* Mon Dec 11 2017 Peng Wu <pwu@redhat.com> - 20170602-4
- Simplify spec file

* Thu Dec  7 2017 Peng Wu <pwu@redhat.com> - 20170602-3
- Include more fonts and sub package fonts

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 20170602-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jun  7 2017 Peng Wu <pwu@redhat.com> - 20170602-1
- Include Serif fonts

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.004-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Dec  2 2016 Peng Wu <pwu@redhat.com> - 1.004-7
- Rebuilt to fixes the spec file

* Fri Dec  2 2016 Peng Wu <pwu@redhat.com> - 1.004-6
- Disable Obsoletes for epel: for google-noto-sans-cjk-fonts (rh#1396260)
- Disable notocjkrep macro definition for epel

* Fri Apr 29 2016 Peng Wu <pwu@redhat.com> - 1.004-5
- Replace google-noto-sans-cjk-fonts package

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.004-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Nov 13 2015 Peng Wu <pwu@redhat.com> - 1.004-3
- Use TTC Files

* Mon Oct 26 2015 Peng Wu <pwu@redhat.com> - 1.004-2
- Fixes Spec

* Mon Oct 26 2015 Peng Wu <pwu@redhat.com> - 1.004-1
- Initial Version
